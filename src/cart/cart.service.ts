import {Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {Cart} from "./interfaces/cart.interface";

@Injectable()
export class CartService {
    constructor(@InjectModel('Cart') private cartModel: Model<Cart>) {
    }

    async create(userId: string): Promise<Cart> {
        const createdCart = new this.cartModel({user: userId, status: 'active', content: []});
        return createdCart.save();
    }

    async findAll(populate: boolean = false): Promise<Cart[]> {
        const carts = this.cartModel.find();
        if (populate) {
            carts.populate('user')
            carts.populate('content')
        }
        return carts.exec();
    }

    async find(cartId: string, populate: boolean = false): Promise<Cart> {
        const cart = this.cartModel.findOne({_id: cartId});
        if (populate) {
            cart.populate('user')
            cart.populate('content')
        }
        return await cart.exec();
    }

    async findActiveByUser(userId: string, populate: boolean = false): Promise<Cart> {
        const cart = this.cartModel.findOne({user: userId, status: 'active'});
        if (populate) {
            cart.populate('user')
            cart.populate('content')
        }
        return await cart.exec();
    }

    async delete(cartId: string): Promise<any> {
        const cart = this.cartModel.deleteOne({_id: cartId})
        return await cart.exec();
    }

    async deleteAll(onlyActive: boolean) {
        if(onlyActive){
            await this.cartModel.deleteMany({status: 'active'});
        }else{
            await this.cartModel.deleteMany({});
        }
    }

    async findAllActive(populate: boolean = false): Promise<Cart[]> {
        const cart = this.cartModel.find({status: 'active'});
        if (populate) {
            cart.populate('user')
            cart.populate('content')
        }
        return await cart.exec();
    }

    async getTopArticles(limitNumber: number, populate: boolean = false) {
        const result = this.cartModel.aggregate();

        result.unwind('$content' );
        result.group({ _id: "$content", count: { $sum: 1 } } );
        result.sort({ count: -1 } );

        if(limitNumber != 0) {
            result.limit(limitNumber);
        }

        if(populate) {
            result.lookup({from: 'articles', localField: '_id', foreignField: '_id', as: 'article'})
        }

        return await result.exec();
    }

    async getTopUsers(limitNumber: number, populate: boolean = false) {
        const result = this.cartModel.aggregate();

        result.project({ "user": 1, "cart_size": { $size: "$content" } } );
        result.group({ _id: "$user", count: { $sum: "$cart_size" } } );
        result.sort({ count: -1 } );

        if(limitNumber != 0) {
            result.limit(limitNumber);
        }

        if(populate) {
            result.lookup({from: 'users', localField: '_id', foreignField: '_id', as: 'user'})
        }

        return await result.exec();

    }

    async findAllValidated(): Promise<Cart[]> {
        const result = this.cartModel.find({status: 'validated'})
        return await result.exec();
    }

    async findValidatedByUser(userId: string, populate: boolean = false): Promise<Cart[]> {
        const carts = this.cartModel.find({user: userId, status: 'validated'});

        if(populate){
            carts.populate('user');
            carts.populate('content');
        }

        return await carts.exec();
    }
}
