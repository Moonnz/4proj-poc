import {Document} from "mongoose";

export interface Cart extends Document {
    user: string;
    status: string;
    content: string[];
    readonly __v: number;
    readonly _id: string;
}
