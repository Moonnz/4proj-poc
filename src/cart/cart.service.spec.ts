import {Test, TestingModule} from '@nestjs/testing';
import {CartService} from './cart.service';
import {MongooseModule} from "@nestjs/mongoose";
import {CartSchema} from "./schemas/cart.schema";

describe('CartService', () => {
    let service: CartService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                MongooseModule.forRoot(process.env.MONGO_URL, {
                    useFindAndModify: false,
                }),
                MongooseModule.forFeature([{name: 'Cart', schema: CartSchema}]),
            ],
            providers: [CartService],
        }).compile();

        service = module.get<CartService>(CartService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
