import {OnGatewayConnection, WebSocketGateway, WebSocketServer,} from '@nestjs/websockets';
import {Server, Socket} from 'socket.io';
import {Inject} from "@nestjs/common";
import {CartService} from "../cart.service";

@WebSocketGateway()
export class CartGateway implements OnGatewayConnection{
    @WebSocketServer() server: Server;
    @Inject() cartService: CartService

    async handleConnection(socket: Socket, ...args: any[]) {
        await this.sendCartsTo(socket);
    }

    async sendCartsTo(socket: Socket) {
        socket.emit('activeCarts', await this.cartService.findAllActive(true));
    }

    async sendCartsToEveryOne() {
        this.server.emit('activeCarts', await this.cartService.findAllActive(true));
    }

}
