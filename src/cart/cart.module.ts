import {Module} from '@nestjs/common';
import {CartService} from "./cart.service";
import {MongooseModule} from "@nestjs/mongoose";
import {CartSchema} from "./schemas/cart.schema";
import {CartGateway} from "./websocket/cart.gateway";

@Module({
    imports: [
        MongooseModule.forFeature([{name: 'Cart', schema: CartSchema}]),
    ],
    providers: [CartService, CartGateway],
    exports: [
        MongooseModule.forFeature([{name: 'Cart', schema: CartSchema}]),
        CartService,
        CartGateway,
    ],
})
export class CartModule {
}
