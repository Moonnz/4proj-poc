import * as mongoose from 'mongoose';

export const CartSchema = new mongoose.Schema({
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    status: String,
    content: [{type: mongoose.Schema.Types.ObjectId, ref: 'Article', required: true}],
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});
