import {Test, TestingModule} from '@nestjs/testing';
import {ArticleController} from './article.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {ArticleSchema} from "./schemas/article.schema";
import {CategorySchema} from "../category/schemas/category.schema";
import {CategoryModule} from "../category/category.module";
import {ArticleService} from "./article.service";
import {CategoryService} from "../category/category.service";

describe('Article Controller', () => {
    let controller: ArticleController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                MongooseModule.forRoot(process.env.MONGO_URL, {
                    useFindAndModify: false,
                }),
                MongooseModule.forFeature([{name: 'Article', schema: ArticleSchema}]),
                MongooseModule.forFeature([{name: 'Category', schema: CategorySchema}]),
                CategoryModule,
            ],
            providers: [ArticleService, CategoryService],
            controllers: [ArticleController],
        }).compile();

        controller = module.get<ArticleController>(ArticleController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
