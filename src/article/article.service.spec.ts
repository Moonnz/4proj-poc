import {Test, TestingModule} from '@nestjs/testing';
import {ArticleService} from './article.service';
import {MongooseModule} from "@nestjs/mongoose";
import {ArticleSchema} from "./schemas/article.schema";
import {CategorySchema} from "../category/schemas/category.schema";
import {CategoryModule} from "../category/category.module";

describe('ArticleService', () => {
    let service: ArticleService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                MongooseModule.forRoot(process.env.MONGO_URL, {
                    useFindAndModify: false,
                }),
                MongooseModule.forFeature([{name: 'Article', schema: ArticleSchema}]),
                MongooseModule.forFeature([{name: 'Category', schema: CategorySchema}]),
                CategoryModule,
            ],
            providers: [ArticleService],
        }).compile();

        service = module.get<ArticleService>(ArticleService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
