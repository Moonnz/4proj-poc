import {Module} from '@nestjs/common';
import {ArticleService} from './article.service';
import {ArticleController} from './article.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {ArticleSchema} from "./schemas/article.schema";
import {CategorySchema} from "../category/schemas/category.schema";
import {CategoryModule} from "../category/category.module";

@Module({
    imports: [
        MongooseModule.forFeature([{name: 'Article', schema: ArticleSchema}]),
        MongooseModule.forFeature([{name: 'Category', schema: CategorySchema}]),
        CategoryModule,
    ],
    providers: [ArticleService],
    controllers: [ArticleController],
    exports: [
        MongooseModule.forFeature([{name: 'Article', schema: ArticleSchema}]),
        MongooseModule.forFeature([{name: 'Category', schema: CategorySchema}]),
        ArticleService,
    ],
})
export class ArticleModule {
}
