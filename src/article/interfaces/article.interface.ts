import {Document} from "mongoose";

export interface Article extends Document {
    name: string;
    categories: string[];
    readonly __v: number;
    readonly _id: string;
}
