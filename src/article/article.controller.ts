import {Body, Controller, Delete, Get, NotFoundException, Param, Post, Put, Query, Res} from '@nestjs/common';
import {ArticleService} from "./article.service";
import {CategoryService} from "../category/category.service";
import {CreateArticleDto} from "./dto/create-article.dto";
import {ApiQuery, ApiTags} from "@nestjs/swagger";

@ApiTags('Article')
@Controller('article')
export class ArticleController {
    constructor(
        private articleService: ArticleService,
        private categoryService: CategoryService,
    ) {
    }

    @ApiQuery({name: 'populate', type: 'boolean', required: false})
    @Get()
    async getAllArticles(@Res() res, @Query() query) {
        const articles = await this.articleService.findAll(query.populate && query.populate === 'true');
        return res.json(articles);
    }

    @Post()
    async createArticle(@Res() res, @Body() createArticleDto: CreateArticleDto, @Query() query) {
        for (const categoryId of createArticleDto.categories) {
            if (await this.categoryService.find(categoryId) === null) {
                throw new NotFoundException('Category: \'' + categoryId + '\' doesn\'t exist.');
            }
        }
        const article = await this.articleService.create(createArticleDto);
        return res.json(article);
    }

    @ApiQuery({name: 'populate', type: 'boolean', required: false})
    @Get(':id')
    async getArticle(@Param('id') id: string, @Res() res, @Query() query) {
        const article = await this.articleService.find(id, query.populate && query.populate === 'true');
        if (article === null) {
            throw new NotFoundException('Article: \'' + id + '\' doesn\'t exist.');
        }
        return res.json(article);
    }

    @Delete(':id')
    async deleteArticle(@Param('id') id: string, @Res() res) {
        const deletedArticle = await this.articleService.delete(id);
        if (deletedArticle.deletedCount === 1) {
            return res.json({
                message: 'Article have been deleted',
            });
        } else {
            throw new NotFoundException('Article: \'' + id + '\' doesn\'t exist.');
        }
    }

    @Put(':id')
    async updateArticle(@Param('id') id: string, @Body() createArticleDto: CreateArticleDto, @Res() res) {
        const updatedArticle = await this.articleService.update(id, createArticleDto);
        if (updatedArticle === null) {
            throw new NotFoundException('Article: \'' + id + '\' doesn\'t exist.');
        } else {
            return res.json(updatedArticle);
        }
    }
}
