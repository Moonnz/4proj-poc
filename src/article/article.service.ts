import {Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {Article} from "./interfaces/article.interface";
import {CreateArticleDto} from "./dto/create-article.dto";

@Injectable()
export class ArticleService {
    constructor(@InjectModel('Article') private articleModel: Model<Article>) {
    }

    async create(createArticleDto: CreateArticleDto): Promise<Article> {
        const createdArticle = new this.articleModel(createArticleDto);
        return createdArticle.save();
    }

    async findAll(populate: boolean = false): Promise<Article[]> {
        const articles = this.articleModel.find();
        if (populate) {
            articles.populate('categories')
        }
        return articles.exec();
    }

    async find(articleId: string, populate: boolean = false): Promise<Article> {
        const article = this.articleModel.findOne({_id: articleId});
        if (populate) {
            article.populate('categories')
        }
        return await article.exec();
    }

    async delete(articleId: string): Promise<any> {
        const article = this.articleModel.deleteOne({_id: articleId});
        return await article.exec();
    }

    async update(articleId: string, createArticleDto: CreateArticleDto): Promise<Article> {
        const updatedArticle = this.articleModel.findOneAndUpdate({_id: articleId}, createArticleDto);
        return await updatedArticle.exec();
    }

    async findByName(articleName: string): Promise<Article> {
        const article = this.articleModel.findOne({name: articleName});
        return await article.exec();
    }
}
