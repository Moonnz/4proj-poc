import * as mongoose from 'mongoose';

export const ArticleSchema = new mongoose.Schema({
    name: {type: String, unique: true},
    categories: {type: [mongoose.Schema.Types.ObjectId], ref: 'Category', required: false},
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});
