import {
    MessageBody,
    OnGatewayConnection,
    OnGatewayInit,
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer,
} from '@nestjs/websockets';
import {Server, Socket} from 'socket.io';
import {Inject} from "@nestjs/common";

import {Article} from "./interfaces/article.class"
import {Checkpoint} from "./interfaces/checkpoint.class"
import {Customer} from "./interfaces/customer.class"

import {ShopService} from "../shop/shop.service";
import {StockService} from "../stock/stock.service";
import {ArticleService} from "../article/article.service";
import {CategoryService} from "../category/category.service";
import {UserService} from '../user/user.service';
import { CartService } from 'src/cart/cart.service';

@WebSocketGateway(4000)
export class DemoGateway implements OnGatewayConnection, OnGatewayInit{
    @WebSocketServer() server: Server;
    @Inject() userService: UserService;
    @Inject() shopService: ShopService;
    @Inject() stockService: StockService;
    @Inject() articleService: ArticleService;
    @Inject() categoryService: CategoryService;
    @Inject() cartService: CartService;

    users = []
    checkpoints = [];
    articles;
    myCategories = [];
    lastSend = 0;
    lastSendStock = 0;
    setIntervalCreated = false;
    shop = null;
    stocks = [];
    ready = false;

    async handleConnection(socket: Socket, ...args: any[]) {
        if(!this.setIntervalCreated) {
            setInterval(() =>{
                this.users.forEach((user, index, list) => {
                    user.doSomething(this.shop, this.checkpoints);
                    if (user.leaved && user.inAction > 15) {
                        list.splice(index, 1);
                    }
                });
            }, 25);
            this.setIntervalCreated = true;
        }
        setInterval(() => {
            if (this.ready && (this.users.length > 0 || this.lastSend < 100)) {
                socket.emit("users", getUsersForVue(this.users));
                if (this.users.length == 0) {
                    this.lastSend++;
                }
            }
        }, 25);
        
        setInterval(async () => {
            if (this.ready && (this.users.length > 0 || this.lastSendStock < 100)) {
                let stocks = await this.stockService.findForShop(this.shop.id, true);
                socket.emit("stocks", getStocksForVue(stocks, this.myCategories));
                if (this.users.length == 0) {
                    this.lastSendStock++;
                }
            }
        }, 1000);
    }

    async afterInit() {
        this.shop = (await this.shopService.findAll())[0];

        let articles = [
            new Article("Broccoli", 0),
            new Article("Carrot", 0),
            new Article("Corn", 0),
            new Article("Cucumber", 0),
            new Article("Lettuce", 0),
            new Article("Onion", 0),
            new Article("Potatoe", 0),
            new Article("Tomatoe", 0),

            new Article("Apple", 1),
            new Article("Banana", 1),
            new Article("Berries", 1),
            new Article("Kiwi", 1),
            new Article("Lemon", 1),
            new Article("Orange", 1),
            new Article("Pear", 1),

            new Article("Fish stick", 2),
            new Article("Ice cream", 2),
            new Article("Pizza", 2),
            new Article("Popsicle", 2),
            new Article("Fries", 2),
            new Article("Vegetables", 2),

            new Article("BBQ sauce", 3),
            new Article("Honey", 3),
            new Article("Hot Sauce", 3),
            new Article("Ketchup", 3),
            new Article("Salsa", 3),
            new Article("Syrup", 3),

            new Article("Cheddar", 4),
            new Article("Feta", 4),
            new Article("Mozzarella", 4),
            new Article("Parmesan", 4),
            new Article("Ricotta", 4),

            new Article("Bacon", 5),
            new Article("Sausage", 5),
            new Article("Beef", 5),
            new Article("Chicken", 5),
            new Article("Ham", 5),
            new Article("Pork", 5),

            new Article("Crab", 6),
            new Article("Lobster", 6),
            new Article("Salmon", 6),
            new Article("Tuna", 6),

            new Article("Rum", 7),
            new Article("Beer", 7),
            new Article("Vodka", 7),
            new Article("Whiskey", 7),
            new Article("Wine", 7),
        ]

        for (let index = 0; index < articles.length; index++) {
            const article = articles[index];

            let categoryExist = await this.categoryService.findByName(article.category.toString());
            if (categoryExist == undefined) {
                await this.categoryService.create({
                    name: article.category.toString()
                })
                categoryExist = await this.categoryService.findByName(article.category.toString());
            }
            if (!this.myCategories.some(cat => cat.id == categoryExist.id)) {
                this.myCategories.push(categoryExist);
            }

            let articleExist = await this.articleService.findByName(article.name);
            if (articleExist == undefined) {
                await this.articleService.create({
                    name: article.name,
                    categories: [ categoryExist.id ]
                });
                articleExist = await this.articleService.findByName(article.name);
            }

            let stock = await this.stockService.findForShopByArticle(this.shop.id, articleExist.id)
            if (stock == undefined) {
                await this.stockService.create({
                    article: articleExist.id,
                    quantity: 2,
                    shop: this.shop.id
                });
            } else {
                this.stockService.update(stock.id, {
                    article: articleExist.id,
                    quantity: 2,
                    shop: this.shop.id
                });
            }
        }
        
        let greenCheckpoint = new Checkpoint("green", 60, 150, 0);
        let purpleCheckpoint = new Checkpoint("purple", 200, 70, 1);
        let orangeCheckpoint = new Checkpoint("orange", 350, 120, 2);
        let redCheckpoint = new Checkpoint("red", 325, 210, 3);
        let greyCheckpoint = new Checkpoint("grey", 415, 310, 4);
        let cyanCheckpoint = new Checkpoint("cyan", 650, 250, 5);
        let yellowCheckpoint = new Checkpoint("yellow", 110, 350, 6);
        let blueCheckpoint = new Checkpoint("blue", 200, 530, 7);
        let entryCheckpoint = new Checkpoint("entry", 700, 650, -1);

        this.checkpoints = [
            new Checkpoint("0", 110, 150),
            new Checkpoint("1", 200, 150),
            new Checkpoint("2", 350, 150),
            new Checkpoint("3", 200, 350),
            new Checkpoint("4", 350, 310),
            new Checkpoint("5", 350, 350),
            new Checkpoint("6", 200, 500),
            new Checkpoint("7", 350, 500),
            new Checkpoint("8", 650, 500),
            new Checkpoint("9", 700, 500),
            new Checkpoint("10", 650, 150),
            greenCheckpoint,
            purpleCheckpoint,
            orangeCheckpoint,
            redCheckpoint,
            greyCheckpoint,
            cyanCheckpoint,
            yellowCheckpoint,
            blueCheckpoint,
            entryCheckpoint
        ];

        greenCheckpoint.addNeighbor(this.checkpoints[0]);

        purpleCheckpoint.addNeighbor(this.checkpoints[1]);

        orangeCheckpoint.addNeighbor(this.checkpoints[2]);

        redCheckpoint.addNeighbor(this.checkpoints[2]);
        redCheckpoint.addNeighbor(this.checkpoints[4]);

        greyCheckpoint.addNeighbor(this.checkpoints[4]);

        cyanCheckpoint.addNeighbor(this.checkpoints[8]);
        cyanCheckpoint.addNeighbor(this.checkpoints[10]);

        yellowCheckpoint.addNeighbor(this.checkpoints[0]);
        yellowCheckpoint.addNeighbor(this.checkpoints[3]);

        blueCheckpoint.addNeighbor(this.checkpoints[6]);

        entryCheckpoint.addNeighbor(this.checkpoints[9]);

        this.checkpoints[0].addNeighbor(greenCheckpoint);
        this.checkpoints[0].addNeighbor(yellowCheckpoint);
        this.checkpoints[0].addNeighbor(this.checkpoints[1]);

        this.checkpoints[1].addNeighbor(purpleCheckpoint);
        this.checkpoints[1].addNeighbor(this.checkpoints[0]);
        this.checkpoints[1].addNeighbor(this.checkpoints[2]);

        this.checkpoints[2].addNeighbor(orangeCheckpoint);
        this.checkpoints[2].addNeighbor(redCheckpoint);
        this.checkpoints[2].addNeighbor(this.checkpoints[10]);

        this.checkpoints[3].addNeighbor(yellowCheckpoint);
        this.checkpoints[3].addNeighbor(this.checkpoints[5]);
        this.checkpoints[3].addNeighbor(this.checkpoints[6]);

        this.checkpoints[4].addNeighbor(redCheckpoint);
        this.checkpoints[4].addNeighbor(greyCheckpoint);
        this.checkpoints[4].addNeighbor(this.checkpoints[5]);

        this.checkpoints[5].addNeighbor(this.checkpoints[3]);
        this.checkpoints[5].addNeighbor(this.checkpoints[4]);
        this.checkpoints[5].addNeighbor(this.checkpoints[7]);

        this.checkpoints[6].addNeighbor(blueCheckpoint);
        this.checkpoints[6].addNeighbor(this.checkpoints[3]);
        this.checkpoints[6].addNeighbor(this.checkpoints[7]);

        this.checkpoints[7].addNeighbor(this.checkpoints[5]);
        this.checkpoints[7].addNeighbor(this.checkpoints[6]);
        this.checkpoints[7].addNeighbor(this.checkpoints[8]);

        this.checkpoints[8].addNeighbor(cyanCheckpoint);
        this.checkpoints[8].addNeighbor(this.checkpoints[7]);
        this.checkpoints[8].addNeighbor(this.checkpoints[9]);

        this.checkpoints[9].addNeighbor(entryCheckpoint);
        this.checkpoints[9].addNeighbor(this.checkpoints[8]);

        this.checkpoints[10].addNeighbor(cyanCheckpoint);
        this.checkpoints[10].addNeighbor(this.checkpoints[2]);

        this.ready = true;
        console.log("I'm ready");
    }

    @SubscribeMessage('addRandomUser')
    handleNewUser(client: Socket, @MessageBody() body: any) {
        if (!this.ready) { return; }

        this.lastSend = 0;
        this.lastSendStock = 0;
        this.userService.findAll().then(users => {
            if (this.users.length == users.length) { return; }
            let userAdded = false;
            while (!userAdded) {
                let randomUser = users[Math.floor(Math.random() * Math.floor(users.length))];
                if (!this.users.find(user => user.id == randomUser.id)) {
                    userAdded = true;
                    this.users.push(new Customer(randomUser.id, randomUser.firstname, randomUser.lastname, this.cartService, this.categoryService, this.articleService, this.stockService));
                }
            }
        });        
    }
    @SubscribeMessage('removeLastUser')
    handleRemoveUser(client: Socket, @MessageBody() body: any) {
        this.users.pop();
    }
}

function getUsersForVue(users) {
    let usersForVue = [];
    users.forEach(user => {
        usersForVue.push({
            inAction: user.inAction,
            coord: user.coord,
            color: user.color,
            fullName: user.fullName,
            cartPopulate: user.cartPopulate,
            addArticle: user.addArticle
        });
    });
    return usersForVue;
}

function getStocksForVue(stocks, categories) {    
    let stocksForVue = [];
    stocks.forEach(stock => {
        if (stock.article == null ||
            stock.article.categories.length == 0 ||
            !categories.some(cat => cat.id == stock.article.categories[0])) {
                return;
        }
        stocksForVue.push({
            id: stock.id,
            article: stock.article.name,
            category: categories.find(cat => cat.id == stock.article.categories[0]).name,
            quantity: stock.quantity
        });
    });

    return groupBy(stocksForVue, stock => stock.category);
}

function groupBy(list, keyGetter) {
    let categoryColor = [
        { name: "Vegetables", color: "#23B14D" },
        { name: "Fruits", color: "#A349A3" },
        { name: "Frozen", color: "#F38D65" },
        { name: "Sauce", color: "#ED1B24" },
        { name: "Cheese", color: "#C3C3C3" },
        { name: "Meat", color: "#ACEBD6" },
        { name: "Seafood", color: "#F2BF56" },
        { name: "Alcool", color: "#00A3E7" }
    ]

    const group = [];
    list.forEach((item) => {
         const key = keyGetter(item);
         const listId = group.findIndex(g => g.key == key);
         if (listId == -1) {
             group.push({
                 key: key,
                 name: categoryColor[parseInt(key)].name,
                 color: categoryColor[parseInt(key)].color,
                 articles: [{
                     name: item.article,
                     quantity: item.quantity
                 }]
             });
         } else {
             group[listId].articles.push({
                name: item.article,
                quantity: item.quantity
             });
         }
    });
    return group;
}