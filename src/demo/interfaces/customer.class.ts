import {Inject} from "@nestjs/common";
import {ShopService} from "../../shop/shop.service";
import {StockService} from "../../stock/stock.service";
import {ArticleService} from "../../article/article.service";
import {CategoryService} from "../../category/category.service";
import {UserService} from "src/user/user.service";
import {CartService} from "src/cart/cart.service";

export class Customer {
	id: string;
	color: string;
	fullName: string;
	inAction: number;
	newUser: boolean;
	coord: any;
	objectif: any;
	cart: any;
	cartPopulate: any;
	objectifCpCoord: any;
	step: any[];
	currentStep: number;
	currentCpCoord: any
	entered: boolean;
	addArticle: boolean;
	leaved: boolean;
	cartService: CartService;
	categoryService: CategoryService;
	articleService: ArticleService;
	stockService: StockService;

	constructor(id, firstname, lastname, cartService, categoryService, articleService, stockService) {
		this.id = id;
		this.fullName = firstname + " " + lastname;
		this.color = this.getRandomColor();
		this.inAction = 0;
		this.newUser = true;
		this.cartService = cartService;
		this.categoryService = categoryService;
		this.articleService = articleService;
		this.stockService = stockService;

		let apparatitionZone = [0, 650, 782, 912];

		// Random the spawning position in the spawning zone
		this.coord = {
			x: this.getRandomInt(apparatitionZone[0], apparatitionZone[2] - 20),
			y: this.getRandomInt(apparatitionZone[1], apparatitionZone[3] - 20)
		};

		this.objectif = {
			x: this.coord.x,
			y: this.coord.y
		};

		this.cart = [];
	}

	// Random int between min and max - 1
	getRandomInt(min: number, max: number) {
		return Math.floor(Math.random() * Math.floor(max - min) + min);
	};

	getRandomColor(): string {
		let letters = '0123456789ABCDEF';
		let color = '#';
		for (let i = 0; i < 6; i++) {
			color += letters[Math.floor(Math.random() * 16)];
		}
		return color;
	};

	// Move to the objective (x then y)
	moveTo(objectif: any) {
		if (objectif.x != this.coord.x) {
			if (Math.abs(this.coord.x - objectif.x) < 5) {
				this.coord.x = objectif.x;
			} else if (objectif.x < this.coord.x) {
				this.coord.x -= 5;
			} else {
				this.coord.x += 5;
			}
		} else {
			if (Math.abs(this.coord.y - objectif.y) < 5) {
				this.coord.y = objectif.y;
			} else if (objectif.y < this.coord.y) {
				this.coord.y -= 5;
			} else {
				this.coord.y += 5;
			}
		}
	};

	// Will do the moving choice
	async doSomething(shop: any, checkpoints: any[]) {
		// If we spawned let's go to the entry door
		if (this.newUser == true) {
			// Get the entry checkpoint
			let objectifCp = checkpoints.find(cp => cp.id == "entry");
			this.objectifCpCoord = objectifCp.coord;

			// Set the step to the entry
			this.step = ("," + objectifCp.id).split(',');
			this.currentStep = 1;
			this.currentCpCoord = objectifCp.coord;

			this.newUser = false;
			this.entered = false;
			this.addArticle = true;
		}

		// If we are at the objective coordinate
		if (this.currentCpCoord.x == this.coord.x && this.currentCpCoord.y == this.coord.y) {
			// If we have finish the step (last objective)
			if (this.step.length == this.currentStep + 1) {
				// If we are adding or removing an article
					this.inAction++;
				if (this.inAction == 1) {
					// If we are entering/leaving the shop
					if (this.step[this.currentStep] == "entry") {
						if (!this.entered) {
							this.cart = await this.cartService.create(this.id);
							this.cartPopulate = this.cart;
							this.entered = true;
						} else {
							if (this.cart.content.length === 0) {
								this.cartService.delete(this.cart.id);
							} else {
								this.cart.status = 'validated';
								await this.cart.save();
							}
							this.addArticle = false;
							this.leaved = true;
						}
					} else if (this.addArticle) {
						// Add an article to the cart
						const categoryNumber = checkpoints.find(cp => cp.id == this.step[this.currentStep]).category;
						const category = await this.categoryService.findByName(categoryNumber.toString());
						const articles = await this.articleService.findAll();
						const stocks = await this.stockService.findForShop(shop.id);
						const articlesWithCategory = articles.filter(article => article.categories.some(cat => cat == category.id));
						const stocksWithCategory = stocks.filter(stock => articlesWithCategory.some(art => art.id == stock.article) &&
																		  stock.quantity > 0);
						if (stocksWithCategory.length > 0) {
							const randomStock = stocksWithCategory[this.getRandomInt(0, stocksWithCategory.length)];
							randomStock.quantity -= 1;
							randomStock.save();
							this.cart.content.push(randomStock.article);
							await this.cart.save();
							this.cartPopulate = await this.cartService.find(this.cart.id, true);
						}						
					} else if (!this.addArticle) {
						const categoryNumber = checkpoints.find(cp => cp.id == this.step[this.currentStep]).category;
						const category = await this.categoryService.findByName(categoryNumber.toString());
						const articles = await this.articleService.findAll();
						const articlesWithCategory = articles.filter(article => article.categories.some(cat => cat == category.id));
						const randomArticle = await this.cart.content.find(art => articlesWithCategory.some(artWithCat => artWithCat.id == art));
						const indexRandomArticle = this.cart.content.indexOf(randomArticle);

						const stock = await this.stockService.findForShopByArticle(shop.id, randomArticle);

						// Remove an article from the cart (we already have check that there are an article of this category/checkpoint)
						this.cart.content.splice(indexRandomArticle, 1);
						stock.quantity += 1;
						stock.save();
						await this.cart.save();
						this.cartPopulate = await this.cartService.find(this.cart.id, true);
					}
				} else if (this.inAction == 20) {
					await this.newObjectif(checkpoints);
					this.inAction = 0;
				}
			// Move to the next step
			} else {
				this.currentStep++;
				this.currentCpCoord = checkpoints.find(cp => cp.id == this.step[this.currentStep]).coord;
				this.moveTo(this.currentCpCoord);
			}
		// Move to the objective
		} else {
			this.moveTo(this.currentCpCoord);
		}
	}

	// Set a new objective
	async newObjectif(checkpoints) {
		let objectifCp;

		// Random to remove an article if the cart is not empty (10%)
		if (Math.random() <= 0.1 && this.cart.content.length > 0) {
			// Choose a random article
			const randomArticle = await this.articleService.find(this.cart.content[this.getRandomInt(0, this.cart.content.length)]);
			const articleCategory = await this.categoryService.find(randomArticle.categories[0]);
			// Get the destination from the category

			objectifCp = checkpoints.find(cp => cp.category == parseInt(articleCategory.name));
			this.addArticle = false;
		} else {
			objectifCp = checkpoints[this.getRandomInt(11, checkpoints.length)];
			this.addArticle = true;
		}

		let actualPosition = checkpoints.find(cp => cp.id == this.step[this.currentStep]);
		this.step = actualPosition.getShortRoute(actualPosition.id, objectifCp.id, actualPosition.id, 0).step.split(',');
		this.currentStep = this.step.length == 1 ? 0 : 1;
		this.currentCpCoord = checkpoints.find(cp => cp.id == this.step[this.currentStep]).coord;
	}
}
