import {Inject} from "@nestjs/common";
import {ShopService} from "../../shop/shop.service";
import {StockService} from "../../stock/stock.service";
import {ArticleService} from "../../article/article.service";
import {CategoryService} from "../../category/category.service";

export class Checkpoint {
    id: string;
    x: number;
    y: number;
    category: number;
    coord: object;
    neighbors: any[];

    @Inject() shopService: ShopService;
    @Inject() stockService: StockService;
    @Inject() articleService: ArticleService;
    @Inject() categoryService: CategoryService;
    @Inject() cartService: ShopService;

    constructor(id: string, x: number, y: number, category: number = 0) {
        this.coord = {
            x: x,
            y: y
        };
        this.neighbors = [];
        this.id = id;
        this.category = category;
    }

    // Add a neighbor to this checkpoint
    addNeighbor(neighbor: any) {
        this.neighbors.push(neighbor);
    }

    // Get the shortest route to another checkpoint
    getShortRoute(id: string, idObjective: string, step: any, distance: number) {
        // Exit door
        if (distance > 20) {
            return { step, distance: distance };
        }

        // If the objective is me, send the steps
        if (this.id == idObjective) {
            return { step: step, distance: distance + 1 };
        }

        // Check the route for each neighbor
        let myRoutes = [];
        this.neighbors.forEach(neighbor => {
            if (neighbor.id != id) {
                myRoutes.push(neighbor.getShortRoute(this.id, idObjective, step + "," + neighbor.id, distance + 1));
            }
        });

        // If no neighbor (except sender) send max distance
        if (myRoutes.length == 0) {
            return { step: step, distance: 21 };
        }

        // Send the shortest route
        return myRoutes.reduce(function(prev, curr) {
            return prev.distance < curr.distance ? prev : curr;
        });
    }
}
