import {Inject} from "@nestjs/common";
import {ShopService} from "../../shop/shop.service";
import {StockService} from "../../stock/stock.service";
import {ArticleService} from "../../article/article.service";
import {CategoryService} from "../../category/category.service";

export class Article {
    name: string;
    category: number;

    @Inject() shopService: ShopService;
    @Inject() stockService: StockService;
    @Inject() articleService: ArticleService;
    @Inject() categoryService: CategoryService;
    @Inject() cartService: ShopService;

    constructor(name: string, category: number) {
        this.name = name;
        this.category = category;
    }
}
