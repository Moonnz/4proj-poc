import {Module} from '@nestjs/common';
import {DemoGateway} from './demo.gateway';
import {ArticleModule} from "../article/article.module";
import {ShopModule} from "../shop/shop.module";
import {CategoryModule} from "../category/category.module";
import {StockModule} from "../stock/stock.module";
import {CartModule} from "../cart/cart.module";
import {UserModule} from 'src/user/user.module';

@Module({
    imports: [
        UserModule,
        ShopModule,
        ArticleModule,
        CategoryModule,
        CartModule,
        StockModule
    ],
    controllers: [],
    providers: [DemoGateway],
})
export class DemoModule {
}
