import {Test, TestingModule} from '@nestjs/testing';
import {CategoryController} from './category.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {CategorySchema} from "./schemas/category.schema";
import {CategoryService} from "./category.service";

describe('Category Controller', () => {
    let controller: CategoryController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                MongooseModule.forRoot(process.env.MONGO_URL, {
                    useFindAndModify: false,
                }),
                MongooseModule.forFeature([{name: 'Category', schema: CategorySchema}])
            ],
            providers: [CategoryService],
            controllers: [CategoryController],
        }).compile();

        controller = module.get<CategoryController>(CategoryController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
