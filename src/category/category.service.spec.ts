import {Test, TestingModule} from '@nestjs/testing';
import {CategoryService} from './category.service';
import {MongooseModule} from "@nestjs/mongoose";
import {CategorySchema} from "./schemas/category.schema";

describe('CategoryService', () => {
    let service: CategoryService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                MongooseModule.forRoot(process.env.MONGO_URL, {
                    useFindAndModify: false,
                }),
                MongooseModule.forFeature([{name: 'Category', schema: CategorySchema}])
            ],
            providers: [CategoryService],
        }).compile();

        service = module.get<CategoryService>(CategoryService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
