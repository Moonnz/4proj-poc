import {Body, Controller, Delete, Get, NotFoundException, Param, Post, Put, Res} from '@nestjs/common';
import {CategoryService} from "./category.service";
import {CreateCategoryDto} from "./dto/create-category.dto";
import {ApiTags} from "@nestjs/swagger";

@ApiTags('Category')
@Controller('category')
export class CategoryController {
    constructor(private categoryService: CategoryService) {
    }

    @Get()
    async getAllCategories(@Res() res) {
        const categories = await this.categoryService.findAll();
        return res.json(categories);
    }

    @Post()
    async createCategory(@Res() res, @Body() createCategoryDto: CreateCategoryDto) {
        const category = await this.categoryService.create(createCategoryDto);
        return res.json(category);
    }

    @Get(':id')
    async getUser(@Param('id') id: string, @Res() res) {
        const category = await this.categoryService.find(id);
        if (category === null) {
            throw new NotFoundException('Category: \'' + id + '\' doesn\'t exist.');
        }
        return res.json(category);
    }

    @Delete(':id')
    async deleteCategory(@Param('id') id: string, @Res() res) {
        const deletedCategory = await this.categoryService.delete(id);
        if (deletedCategory.deletedCount === 1) {
            return res.json({
                message: 'Category have been deleted',
            });
        } else {
            throw new NotFoundException('Category: \'' + id + '\' doesn\'t exist.');
        }
    }

    @Put(':id')
    async updateCategory(@Param('id') id: string, @Body() createCategoryDto: CreateCategoryDto, @Res() res) {
        const updatedCategory = await this.categoryService.update(id, createCategoryDto);
        if (updatedCategory === null) {
            throw new NotFoundException('User: \'' + id + '\' doesn\'t exist.');
        } else {
            return res.json(updatedCategory);
        }
    }
}

