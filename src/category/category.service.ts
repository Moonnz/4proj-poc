import {Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Category} from "./interfaces/category.interface";
import {Model} from "mongoose";
import {CreateCategoryDto} from "./dto/create-category.dto";

@Injectable()
export class CategoryService {
    constructor(@InjectModel('Category') private categoryModel: Model<Category>) {
    }

    async create(createCategoryDto: CreateCategoryDto): Promise<Category> {
        const createdCategory = new this.categoryModel(createCategoryDto);
        return createdCategory.save();
    }

    async findAll(): Promise<Category[]> {
        return this.categoryModel.find().exec();
    }

    async find(categoryId: string): Promise<Category> {
        const category = this.categoryModel.findOne({_id: categoryId});
        return await category.exec();
    }

    async delete(categoryId: string): Promise<any> {
        const category = this.categoryModel.deleteOne({_id: categoryId});
        return await category.exec();
    }

    async update(categoryId: string, createCategoryDto: CreateCategoryDto): Promise<Category> {
        const updatedCategory = this.categoryModel.findOneAndUpdate({_id: categoryId}, createCategoryDto);
        return await updatedCategory.exec();
    }

    async findByName(categoryName: string): Promise<Category> {
        const category = this.categoryModel.findOne({name: categoryName});
        return await category.exec();
    }
}
