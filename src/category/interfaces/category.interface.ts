import {Document} from 'mongoose';

export interface Category extends Document {
    name: string;
    readonly __v: number;
    readonly _id: string;
}
