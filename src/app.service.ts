import {Injectable} from '@nestjs/common';

@Injectable()
export class AppService {
    getVersion(): string {
        if(process.env.VERSION && process.env.BUILD_DATE) {
            return process.env.VERSION + ' at ' + process.env.BUILD_DATE;
        } else {
            return "Unknown";
        }
    }
}
