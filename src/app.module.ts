import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {MongooseModule} from '@nestjs/mongoose';
import {UserModule} from './user/user.module';
import {ShopModule} from './shop/shop.module';
import {CategoryModule} from "./category/category.module";
import {ArticleModule} from './article/article.module';
import {StockModule} from './stock/stock.module';
import {CartModule} from './cart/cart.module';
import {DemoModule} from "./demo/demo.module";

@Module({
    imports: [
        MongooseModule.forRoot(process.env.MONGO_URL, {
            useFindAndModify: false,
        }),
        UserModule,
        ShopModule,
        CategoryModule,
        ArticleModule,
        StockModule,
        CartModule,
        DemoModule,
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
