import {Module} from '@nestjs/common';
import {MongooseModule} from "@nestjs/mongoose";
import {ShopService} from "./shop.service";
import {ShopSchema} from "./schemas/shop.schema";
import {ShopController} from "./shop.controller";
import {UserModule} from "../user/user.module";
import {StockModule} from "../stock/stock.module";
import {ArticleModule} from "../article/article.module";
import {CartModule} from "../cart/cart.module";

@Module({
    imports: [
        MongooseModule.forFeature([{name: 'Shop', schema: ShopSchema}]),
        UserModule,
        StockModule,
        ArticleModule,
        CartModule,
    ],
    providers: [ShopService],
    controllers: [ShopController],
    exports: [
        MongooseModule.forFeature([{name: 'Shop', schema: ShopSchema}]),
        ShopService,
    ]
})
export class ShopModule {
}
