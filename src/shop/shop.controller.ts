import {
    BadRequestException,
    Body,
    Controller,
    Delete,
    Get,
    NotFoundException,
    Param,
    Post,
    Put,
    Query,
    Res
} from '@nestjs/common';
import {ShopService} from "./shop.service";
import {CreateShopDto} from "./dto/create-shop.dto";
import {UserService} from "../user/user.service";
import {CreateStockDto} from "../stock/dto/create-stock.dto";
import {StockService} from "../stock/stock.service";
import {ArticleService} from "../article/article.service";
import {CartService} from "../cart/cart.service";
import {CartGateway} from "../cart/websocket/cart.gateway";
import {ApiQuery, ApiTags} from '@nestjs/swagger';

@ApiTags('Shop')
@Controller('shop')
export class ShopController {
    constructor(
        private shopService: ShopService,
        private userService: UserService,
        private stockService: StockService,
        private articleService: ArticleService,
        private cartService: CartService,
        private cartGateway: CartGateway,
    ) {
    }

    @ApiQuery({name: 'populate', type: 'boolean', required: false})
    @Get()
    async getAllShop(@Res() res, @Query() query) {
        const shops = await this.shopService.findAll(query.populate && query.populate === 'true');
        return res.json(shops);
    }

    @ApiQuery({name: 'populate', type: 'boolean', required: false})
    @ApiQuery({name: 'limit', type: 'number', required: false})
    @Get('/topUsers')
    async topUsers(@Res() res, @Query() query) {
        const result = await this.cartService.getTopUsers(query.limit ? parseInt(query.limit) : 0, query.populate && query.populate === 'true');
        return res.json(result);
    }

    @ApiQuery({name: 'populate', type: 'boolean', required: false})
    @ApiQuery({name: 'limit', type: 'number', required: false})
    @Get('/topArticles')
    async topArticles(@Res() res, @Query() query) {
        const result = await this.cartService.getTopArticles(query.limit ? parseInt(query.limit) : 0, query.populate && query.populate === 'true');
        return res.json(result);
    }

    @Get('/totalValidated')
    async totalValidated(@Res() res, @Query() query) {
        const result = await this.cartService.findAllValidated();
        return res.json({
            total: result.length,
        });
    }

    @Post()
    async createShop(@Res() res, @Body() createShopDto: CreateShopDto) {
        if (await this.userService.find(createShopDto.manager) === null) {
            throw new NotFoundException('User: \'' + createShopDto.manager + '\' doesn\'t exist.');
        } else {
            const shop = await this.shopService.create(createShopDto);
            return res.json(shop);
        }
    }

    @ApiQuery({name: 'populate', type: 'boolean', required: false})
    @Get(':id')
    async getShop(@Param('id') id: string, @Res() res, @Query() query) {
        const shop = await this.shopService.find(id, query.populate && query.populate === 'true');
        if (shop === null) {
            throw new NotFoundException('Shop: \'' + id + '\' doesn\'t exist.');
        }
        return res.json(shop);
    }

    @Delete(':id')
    async deleteShop(@Param('id') id: string, @Res() res) {
        const deletedShop = await this.shopService.delete(id);
        if (deletedShop.deletedCount === 1) {
            return res.json({
                message: 'Shop have been deleted',
            });
        } else {
            throw new NotFoundException('Shop: \'' + id + '\' doesn\'t exist.');
        }
    }

    @Put(':id')
    async updateShop(@Param('id') id: string, @Body() createShopDto: CreateShopDto, @Res() res) {
        const updatedShop = await this.shopService.update(id, createShopDto);
        if (updatedShop === null) {
            throw new NotFoundException('Shop: \'' + id + '\' doesn\'t exist.');
        } else {
            return res.json(updatedShop);
        }
    }

    @ApiQuery({name: 'populate', type: 'boolean', required: false})
    @Get(':id/stock')
    async getStocks(@Param('id') id: string, @Res() res, @Query() query) {
        if (await this.shopService.find(id) === null) {
            throw new NotFoundException('Shop: \'' + id + '\' doesn\'t exist.');
        } else {
            const shopStocks = await this.stockService.findForShop(id, query.populate && query.populate === 'true');
            return res.json(shopStocks);
        }
    }

    @Post(':id/stock')
    async createStock(@Param('id') id: string, @Body() createStockDto: CreateStockDto, @Res() res) {
        if (await this.shopService.find(id) === null) {
            throw new NotFoundException('Shop: \'' + id + '\' doesn\'t exist.');
        } else if (await this.articleService.find(createStockDto.article) === null) {
            throw new NotFoundException('Article: \'' + createStockDto.article + '\' doesn\'t exist.');
        } else {
            const stock = await this.stockService.create(createStockDto);
            return res.json(stock);
        }
    }

    @Post(':id/inClient/:clientId')
    async createCart(@Param('id') id: string, @Param('clientId') clientId: string, @Res() res) {
        if (await this.userService.find(clientId) === null) {
            throw new NotFoundException('User: \'' + clientId + '\' doesn\'t exist.');
        } else {
            const createdCart = await this.cartService.create(clientId);
            this.cartGateway.sendCartsToEveryOne();
            return res.json(createdCart);
        }
    }

    @Put(':id/client/:clientId/addArticle/:articleId')
    async addArticle(@Param('id') id: string, @Param('clientId') clientId: string, @Param('articleId') articleId: string, @Res() res) {
        const cart = await this.cartService.findActiveByUser(clientId);
        if (cart === null) {
            throw new NotFoundException('Cart for user: \'' + clientId + '\' doesn\'t exist.');
        } else if (await this.articleService.find(articleId) === null) {
            throw new NotFoundException('Article: \'' + articleId + '\' doesn\'t exist.');
        } else {
            const stock = await this.stockService.findForShopByArticle(id, articleId);
            if (stock === null) {
                throw new BadRequestException('Article: \'' + articleId + '\' doesn\'t exist in Shop: \'' + id + '\'');
            } else if (stock.quantity === 0) {
                throw new BadRequestException('Article: \'' + articleId + '\' is no longer available in Shop: \'' + id + '\'');
            } else {
                stock.quantity -= 1;
                stock.save();
                cart.content.push(articleId);
                const updatedCart = await cart.save();
                this.cartGateway.sendCartsToEveryOne();
                return res.json(updatedCart);
            }
        }
    }

    @Put(':id/client/:clientId/removeArticle/:articleId')
    async removeArticle(@Param('id') id: string, @Param('clientId') clientId: string, @Param('articleId') articleId: string, @Res() res) {
        const cart = await this.cartService.findActiveByUser(clientId);
        if (cart === null) {
            throw new NotFoundException('Cart for user: \'' + clientId + '\' doesn\'t exist.');
        } else if (await this.articleService.find(articleId) === null) {
            throw new NotFoundException('Article: \'' + articleId + '\' doesn\'t exist.');
        } else {
            const stock = await this.stockService.findForShopByArticle(id, articleId);
            if (stock === null) {
                throw new BadRequestException('Article: \'' + articleId + '\' doesn\'t exist in Shop: \'' + id + '\'');
            } else {
                const index = cart.content.indexOf(articleId);
                if (index > -1) {
                    cart.content.splice(index, 1);
                    stock.quantity += 1;
                    await stock.save()
                    const updatedCart = await cart.save();
                    this.cartGateway.sendCartsToEveryOne();
                    return res.json(updatedCart);
                } else {
                    throw new BadRequestException('Article: \'' + articleId + '\' doesn\'t exist in Cart: \'' + cart._id + '\'')
                }
            }
        }
    }

    @Delete(':id/outClient/:clientId')
    async removeOrValidCart(@Param('id') id: string, @Param('clientId') clientId: string, @Res() res) {
        const cart = await this.cartService.findActiveByUser(clientId);
        if (cart === null) {
            throw new NotFoundException('No active cart found for user: ' + clientId);
        } else if (cart.content.length === 0) {
            const deletedCart = await this.cartService.delete(cart._id);
            if (deletedCart.deletedCount === 1) {
                this.cartGateway.sendCartsToEveryOne();
                return res.json(await this.cartService.find(cart._id))
            } else {
                throw new NotFoundException('Cart: \'' + cart.id + '\' doesn\'t exist.');
            }
        } else {
            cart.status = 'validated';
            const updatedCart = await cart.save();
            this.cartGateway.sendCartsToEveryOne();
            return res.json(updatedCart);
        }
    }
}

