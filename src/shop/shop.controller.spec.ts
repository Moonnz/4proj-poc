import {Test, TestingModule} from '@nestjs/testing';
import {ShopController} from './shop.controller';
import {ShopService} from "./shop.service";
import {MongooseModule} from "@nestjs/mongoose";
import {ShopSchema} from "./schemas/shop.schema";
import {UserSchema} from "../user/schemas/user.schema";
import {StockSchema} from "../stock/schemas/stock.schema";
import {ArticleSchema} from "../article/schemas/article.schema";
import {CartSchema} from "../cart/schemas/cart.schema";
import {UserModule} from "../user/user.module";
import {StockModule} from "../stock/stock.module";
import {ArticleModule} from "../article/article.module";
import {CartModule} from "../cart/cart.module";
import {UserService} from "../user/user.service";
import {StockService} from "../stock/stock.service";
import {ArticleService} from "../article/article.service";
import {CartService} from "../cart/cart.service";
import {CartGateway} from "../cart/websocket/cart.gateway";

describe('Shop Controller', () => {
    let controller: ShopController;
    let service: ShopService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                MongooseModule.forRoot(process.env.MONGO_URL, {
                    useFindAndModify: false,
                }),
                MongooseModule.forFeature([{name: 'Shop', schema: ShopSchema}]),
                MongooseModule.forFeature([{name: 'User', schema: UserSchema}]),
                MongooseModule.forFeature([{name: 'Stock', schema: StockSchema}]),
                MongooseModule.forFeature([{name: 'Article', schema: ArticleSchema}]),
                MongooseModule.forFeature([{name: 'Cart', schema: CartSchema}]),
                UserModule,
                StockModule,
                ArticleModule,
                CartModule,
            ],
            controllers: [ShopController],
            providers: [ShopService, UserService, StockService, ArticleService, CartService, CartGateway],
        }).compile();

        controller = module.get<ShopController>(ShopController);
        service = module.get<ShopService>(ShopService);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
