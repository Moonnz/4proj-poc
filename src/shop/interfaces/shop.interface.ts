import {Document} from "mongoose";

export interface Shop extends Document {
    city: string;
    address: string;
    manager: string;
    readonly __v: number;
    readonly _id: string;
}
