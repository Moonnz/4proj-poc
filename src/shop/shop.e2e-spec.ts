import * as request from 'supertest';
import {Test} from '@nestjs/testing';
import {INestApplication} from '@nestjs/common';
import {MongooseModule} from "@nestjs/mongoose";
import {ShopSchema} from "./schemas/shop.schema";
import {UserSchema} from "../user/schemas/user.schema";
import {StockSchema} from "../stock/schemas/stock.schema";
import {ArticleSchema} from "../article/schemas/article.schema";
import {CartSchema} from "../cart/schemas/cart.schema";
import {UserModule} from "../user/user.module";
import {StockModule} from "../stock/stock.module";
import {ArticleModule} from "../article/article.module";
import {CartModule} from "../cart/cart.module";
import {ShopController} from "./shop.controller";
import {ShopService} from "./shop.service";
import {UserService} from "../user/user.service";
import {StockService} from "../stock/stock.service";
import {ArticleService} from "../article/article.service";
import {CartService} from "../cart/cart.service";
import {User} from "../user/interfaces/user.interface";
import {CartGateway} from "../cart/websocket/cart.gateway";
import assert = require("assert");

describe('Shop', () => {
    let app: INestApplication;
    let controller: ShopController;
    let service: ShopService;
    let userService: UserService;
    let cartService: CartService;
    let stockService: StockService;
    let articleService: ArticleService;

    let toDeleteUserId = [];
    let toDeleleteArticleId = '';

    beforeAll(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [
                MongooseModule.forRoot(process.env.MONGO_URL, {
                    useFindAndModify: false,
                }),
                MongooseModule.forFeature([{name: 'Shop', schema: ShopSchema}]),
                MongooseModule.forFeature([{name: 'User', schema: UserSchema}]),
                MongooseModule.forFeature([{name: 'Stock', schema: StockSchema}]),
                MongooseModule.forFeature([{name: 'Article', schema: ArticleSchema}]),
                MongooseModule.forFeature([{name: 'Cart', schema: CartSchema}]),
                UserModule,
                StockModule,
                ArticleModule,
                CartModule,
            ],
            controllers: [ShopController],
            providers: [ShopService, UserService, StockService, ArticleService, CartService, CartGateway],
        })
            .compile();

        app = moduleRef.createNestApplication();
        controller = app.get<ShopController>(ShopController);
        service = app.get<ShopService>(ShopService);
        userService = app.get<UserService>(UserService);
        cartService = app.get<CartService>(CartService);
        stockService = app.get<StockService>(StockService);
        articleService = app.get<ArticleService>(ArticleService);
        await cartService.deleteAll(true);
        await app.init();
    });

    it(`/GET shop`, async () => {
        const expectedResult = await service.findAll();
        return request(app.getHttpServer())
            .get('/shop')
            .expect(200)
            .expect(JSON.stringify(expectedResult));
    });

    it('/POST shop', async () => {
        const users = await userService.findAll();
        let user = undefined;
        if (users.length === 0) {
            user = await userService.create({
                lastname: "",
                firstname: "",
                password: "",
                city: "",
                address: "",
                phone: "",
                mail: "",
                status: "",
            });
            toDeleteUserId.push(user._id);
        } else {
            user = users[0];
        }

        return request(app.getHttpServer())
            .post('/shop')
            .send({
                "city": "a",
                "adress": "b",
                "manager": user._id,
            })
            .expect(201)
            .then(async response => {
                assert(await service.find(response.body._id) !== null);
            })
    });

    it('/PUT shop', async () => {
        const shops = await service.findAll();
        return request(app.getHttpServer())
            .put('/shop/' + shops[0]._id)
            .send({
                "city": "b",
                "address": "a",
                "manager": shops[0].manager,
            })
            .expect(200)
            .then(async response => {
                const shop = await service.find(response.body._id);
                assert(shop.city === 'b');
                assert(shop.address === 'a');
            })
    });

    describe('Create and Delete empty cart', () => {
        let usedUserId;
        let createdCartId;
        let usedShopId;
        it('/POST :id/inClient/:clientId', async () => {
            const shops = await service.findAll();
            const users = await userService.findAll();

            let userId;

            if (users.length === 0) {
                const user = await userService.create({
                    lastname: "",
                    firstname: "",
                    password: "",
                    city: "",
                    address: "",
                    phone: "",
                    mail: "",
                    status: "",
                });
                userId = user._id
                toDeleteUserId.push(userId);
            } else {
                userId = users[0]._id;
            }
            usedUserId = userId;

            usedShopId = shops[0]._id

            return request(app.getHttpServer())
                .post('/shop/' + usedShopId + '/inClient/' + userId)
                .expect(201)
                .then(async response => {
                    createdCartId = response.body._id;
                    const cart = await cartService.find(createdCartId);
                    expect(cart).not.toBe(null);
                });
        })

        it('/DELETE :id/outClient/:clientId', async () => {
            return request(app.getHttpServer())
                .delete('/shop/' + usedShopId + '/outClient/' + usedUserId)
                .expect(200)
                .then(async response => {
                    expect(await cartService.find(createdCartId)).toBeNull();
                })
        })
    })

    describe('Create and Valid cart', () => {
        let usedUserId;
        let createdCartId;
        let usedShopId;
        let userId;
        let usedArticleId;
        let usedStockId;

        it('/POST :id/inClient/:clientId', async () => {
            const shops = await service.findAll();
            usedShopId = shops[0]._id;

            const users = await userService.findAll();
            if (users.length === 0) {
                const user = await userService.create({
                    lastname: "",
                    firstname: "",
                    password: "",
                    city: "",
                    address: "",
                    phone: "",
                    mail: "",
                    status: "",
                });
                userId = user._id
                toDeleteUserId.push(userId);
            } else {
                userId = users[0]._id;
            }
            usedUserId = userId;


            const article = await articleService.create({
                name: "articleName",
                categories: [],
            });
            const stock = await stockService.create({
                article: article._id,
                quantity: 1,
                shop: usedShopId,
            });

            usedArticleId = article._id;
            toDeleleteArticleId = article._id;
            usedStockId = stock._id;

            return request(app.getHttpServer())
                .post('/shop/' + usedShopId + '/inClient/' + userId)
                .expect(201)
                .then(async response => {
                    createdCartId = response.body._id;
                    const cart = await cartService.find(createdCartId);
                    assert(cart !== null);
                });
        })

        it('/PUT :id/client/:clientId/addArticle/:articleId', async ( ) => {
            return request(app.getHttpServer())
                .put('/shop/'+ usedShopId + '/client/' + usedUserId + '/addArticle/' + usedArticleId)
                .expect(200)
                .then(async response => {
                    const cart = await cartService.find(createdCartId);
                    const stock = await stockService.find(usedStockId);
                    expect(cart).not.toBe(null);
                    expect(cart.content.length).toBe(1);
                    expect(stock.quantity).toBe(0);
                })
        })

        it('/PUT :id/client/:clientId/addArticle/:articleId', async ( ) => {
            return request(app.getHttpServer())
                .put('/shop/'+ usedShopId + '/client/' + usedUserId + '/addArticle/' + usedArticleId)
                .expect(400)
                .then(async response => {
                    const cart = await cartService.find(createdCartId);
                    const stock = await stockService.find(usedStockId);
                    expect(cart).not.toBe(null);
                    expect(cart.content.length).toBe(1);
                    expect(stock.quantity).toBe(0);
                })
        })

        it('/PUT :id/client/:clientId/removeArticle/:articleId', async ( ) => {
            return request(app.getHttpServer())
                .put('/shop/'+ usedShopId + '/client/' + usedUserId + '/removeArticle/' + usedArticleId)
                .expect(200)
                .then(async response => {
                    const cart = await cartService.find(createdCartId);
                    const stock = await stockService.find(usedStockId);
                    expect(cart).not.toBe(null);
                    expect(cart.content.length).toBe(0);
                    expect(stock.quantity).toBe(1);
                })
        })

        it('/PUT :id/client/:clientId/removeArticle/:articleId', async ( ) => {
            return request(app.getHttpServer())
                .put('/shop/'+ usedShopId + '/client/' + usedUserId + '/removeArticle/' + usedArticleId)
                .expect(400)
                .then(async response => {
                    const cart = await cartService.find(createdCartId);
                    const stock = await stockService.find(usedStockId);
                    expect(cart).not.toBe(null);
                    expect(cart.content.length).toBe(0);
                    expect(stock.quantity).toBe(1);
                })
        })

        it('/PUT :id/client/:clientId/addArticle/:articleId', async ( ) => {
            return request(app.getHttpServer())
                .put('/shop/'+ usedShopId + '/client/' + usedUserId + '/addArticle/' + usedArticleId)
                .expect(200)
                .then(async response => {
                    const cart = await cartService.find(createdCartId);
                    const stock = await stockService.find(usedStockId);
                    expect(cart).not.toBe(null);
                    expect(cart.content.length).toBe(1);
                    expect(stock.quantity).toBe(0);
                })
        })

        it('/DELETE :id/outClient/:clientId', async () => {
            return request(app.getHttpServer())
                .delete('/shop/' + usedShopId + '/outClient/' + usedUserId)
                .expect(200)
                .then(async response => {
                    const cart = await cartService.find(createdCartId);
                    expect(cart).not.toBeNull();
                    expect(cart.status).toBe('validated');
                })
        })

        it('/DELETE :id/outClient/:clientId', async () => {
            return request(app.getHttpServer())
                .delete('/shop/' + usedShopId + '/outClient/' + usedUserId)
                .expect(404)
                .then(async response => {
                    const cart = await cartService.find(createdCartId);
                    expect(cart).not.toBeNull();
                    expect(cart.status).toBe('validated');
                })
        })
    })

    afterAll(async () => {
        if (toDeleteUserId.length > 0) {
            for(const id in toDeleteUserId) {
                await userService.delete(id);
            }
        }
        await cartService.deleteAll(true);
        await articleService.delete(toDeleleteArticleId);
        await app.close();
    });
});
