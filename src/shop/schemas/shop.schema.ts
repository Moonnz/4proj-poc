import * as mongoose from 'mongoose';

export const ShopSchema = new mongoose.Schema({
    city: String,
    address: String,
    manager: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});
