import {Model} from 'mongoose';
import {Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {CreateShopDto} from './dto/create-shop.dto';
import {Shop} from './interfaces/shop.interface';

@Injectable()
export class ShopService {
    constructor(@InjectModel('Shop') private shopModel: Model<Shop>) {
    }

    async create(createShopDto: CreateShopDto): Promise<Shop> {
        const createdShop = new this.shopModel(createShopDto);
        return createdShop.save();
    }

    async findAll(populate: boolean = false): Promise<Shop[]> {
        const shops = this.shopModel.find();
        if (populate) {
            shops.populate('user')
        }
        return await shops.exec();
    }

    async find(shopId: string, populate: boolean = false): Promise<Shop> {
        const shop = this.shopModel.findOne({_id: shopId});
        if (populate) {
            shop.populate('user')
        }
        return await shop.exec();
    }

    async delete(shopId: string): Promise<any> {
        const shop = this.shopModel.deleteOne({_id: shopId});
        return await shop.exec();
    }

    async update(shopId: string, createShopDto: CreateShopDto): Promise<Shop> {
        const updatedShop = this.shopModel.findOneAndUpdate({_id: shopId}, createShopDto);
        return await updatedShop.exec();
    }
}
