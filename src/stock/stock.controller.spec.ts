import {Test, TestingModule} from '@nestjs/testing';
import {StockController} from './stock.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {StockSchema} from "./schemas/stock.schema";
import {StockService} from "./stock.service";

describe('Stock Controller', () => {
    let controller: StockController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                MongooseModule.forRoot(process.env.MONGO_URL, {
                    useFindAndModify: false,
                }),
                MongooseModule.forFeature([{name: 'Stock', schema: StockSchema}]),
            ],
            providers: [StockService],
            controllers: [StockController],
        }).compile();

        controller = module.get<StockController>(StockController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
