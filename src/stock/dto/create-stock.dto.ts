import {ApiProperty} from "@nestjs/swagger";
import {IsNotEmpty, IsNumberString, IsString} from "class-validator";

export class CreateStockDto {
    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    article: string;

    @IsNumberString()
    @ApiProperty()
    quantity: number;

    @IsString()
    @IsNotEmpty()
    @ApiProperty()
    shop: string;
}
