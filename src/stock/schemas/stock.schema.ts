import * as mongoose from 'mongoose';

export const StockSchema = new mongoose.Schema({
    article: {type: mongoose.Schema.Types.ObjectId, ref: 'Article', required: true},
    quantity: Number,
    shop: {type: mongoose.Schema.Types.ObjectId, ref: 'Shop', required: true},
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});
