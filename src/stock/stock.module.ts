import {Module} from '@nestjs/common';
import {StockService} from './stock.service';
import {StockController} from './stock.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {StockSchema} from "./schemas/stock.schema";

@Module({
    imports: [
        MongooseModule.forFeature([{name: 'Stock', schema: StockSchema}]),
    ],
    providers: [StockService],
    controllers: [StockController],
    exports: [
        MongooseModule.forFeature([{name: 'Stock', schema: StockSchema}]),
        StockService,
    ]
})
export class StockModule {
}
