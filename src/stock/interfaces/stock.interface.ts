import {Document} from "mongoose";

export interface Stock extends Document {
    article: string;
    quantity: number;
    shop: string;
    readonly __v: number;
    readonly _id: string;
}
