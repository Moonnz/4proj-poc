import {Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {Model} from "mongoose";
import {Stock} from "./interfaces/stock.interface";
import {CreateStockDto} from "./dto/create-stock.dto";

@Injectable()
export class StockService {
    constructor(@InjectModel('Stock') private stockModel: Model<Stock>) {
    }

    async create(createStockDto: CreateStockDto): Promise<Stock> {
        const createdStock = new this.stockModel(createStockDto);
        return createdStock.save();
    }

    async findAll(populate: boolean = false): Promise<Stock[]> {
        const stocks = this.stockModel.find();
        if (populate) {
            stocks.populate('article');
            stocks.populate('shop');
        }
        return stocks.exec();
    }

    async find(stockId: string, populate: boolean = false): Promise<Stock> {
        const stock = this.stockModel.findOne({_id: stockId});
        if (populate) {
            stock.populate('article');
            stock.populate('shop');
        }
        return await stock.exec();
    }

    async delete(stockId: string): Promise<any> {
        const stock = this.stockModel.deleteOne({_id: stockId});
        return await stock.exec();
    }

    async update(stockId: string, createStockDto: CreateStockDto): Promise<Stock> {
        const updatedStock = this.stockModel.findOneAndUpdate({_id: stockId}, createStockDto);
        return await updatedStock.exec();
    }

    async findForShop(shopId: string, populate: boolean = false): Promise<Stock[]> {
        const stocks = this.stockModel.find({shop: shopId});
        if (populate) {
            stocks.populate('article');
            stocks.populate('shop');
        }
        return await stocks.exec();
    }

    async findForShopByArticle(shopId: string, articleId: string, populate: boolean = false): Promise<Stock> {
        const stock = this.stockModel.findOne({shop: shopId, article: articleId})
        if (populate) {
            stock.populate('article');
            stock.populate('shop');
        }
        return await stock.exec();
    }

}
