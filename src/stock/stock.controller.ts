import {Body, Controller, Delete, NotFoundException, Param, Put, Res} from '@nestjs/common';
import {StockService} from "./stock.service";
import {CreateStockDto} from "./dto/create-stock.dto";
import {ApiTags} from "@nestjs/swagger";

@ApiTags('Stock')
@Controller('stock')
export class StockController {
    constructor(
        private stockService: StockService,
    ) {
    }

    @Delete(':id')
    async deleteShop(@Param('id') id: string, @Res() res) {
        const deletedStock = await this.stockService.delete(id);
        if (deletedStock.deletedCount === 1) {
            return res.json({
                message: 'Stock have been deleted',
            });
        } else {
            throw new NotFoundException('Stock: \'' + id + '\' doesn\'t exist.');
        }
    }

    @Put(':id')
    async updateShop(@Param('id') id: string, @Body() createStockDto: CreateStockDto, @Res() res) {
        const updatedStock = await this.stockService.update(id, createStockDto);
        if (updatedStock === null) {
            throw new NotFoundException('Stock: \'' + id + '\' doesn\'t exist.');
        } else {
            return res.json(updatedStock);
        }
    }
}
