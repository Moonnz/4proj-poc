import {Test, TestingModule} from '@nestjs/testing';
import {StockService} from './stock.service';
import {MongooseModule} from "@nestjs/mongoose";
import {StockSchema} from "./schemas/stock.schema";

describe('StockService', () => {
    let service: StockService;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                MongooseModule.forRoot(process.env.MONGO_URL, {
                    useFindAndModify: false,
                }),
                MongooseModule.forFeature([{name: 'Stock', schema: StockSchema}]),
            ],
            providers: [StockService],
        }).compile();

        service = module.get<StockService>(StockService);
    });

    it('should be defined', () => {
        expect(service).toBeDefined();
    });
});
