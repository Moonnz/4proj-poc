import {Test, TestingModule} from '@nestjs/testing';
import {UserController} from './user.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {UserSchema} from "./schemas/user.schema";
import {UserService} from "./user.service";
import {CartModule} from "../cart/cart.module";
import {CartService} from "../cart/cart.service";
import {CartSchema} from "../cart/schemas/cart.schema";

describe('User Controller', () => {
    let controller: UserController;

    beforeEach(async () => {
        const module: TestingModule = await Test.createTestingModule({
            imports: [
                MongooseModule.forFeature([{name: 'User', schema: UserSchema}]),
                MongooseModule.forFeature([{name: 'Cart', schema: CartSchema}]),
                MongooseModule.forRoot(process.env.MONGO_URL, {
                    useFindAndModify: false,
                }),
                CartModule
            ],
            providers: [
                UserService,
                CartService
            ],
            controllers: [UserController],
        }).compile();

        controller = module.get<UserController>(UserController);
    });

    it('should be defined', () => {
        expect(controller).toBeDefined();
    });
});
