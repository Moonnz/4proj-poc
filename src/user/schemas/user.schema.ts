import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
    lastname: String,
    firstname: String,
    password: String,
    city: String,
    address: String,
    phone: String,
    mail: String,
    status: String,
}, {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}});
