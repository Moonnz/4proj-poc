import {Model} from 'mongoose';
import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {User} from './interfaces/user.interface';
import {CreateUserDto} from './dto/create-user.dto';

@Injectable()
export class UserService {
    constructor(@InjectModel('User') private userModel: Model<User>) {
    }

    async create(createUserDto: CreateUserDto): Promise<User> {
        const createdUser = new this.userModel(createUserDto);
        return createdUser.save();
    }

    async findAll(): Promise<User[]> {
        return this.userModel.find().exec();
    }

    async find(userId: string): Promise<User> {
        const user = this.userModel.findOne({_id: userId});
        return await user.exec();
    }

    async delete(userId: string): Promise<any> {
        const user = this.userModel.deleteOne({_id: userId});
        return await user.exec();
    }

    async update(userId: string, createUserDto: CreateUserDto): Promise<User> {
        const updatedRole = this.userModel.findOneAndUpdate({_id: userId}, createUserDto);
        return await updatedRole.exec();
    }
}
