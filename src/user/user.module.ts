import {Module} from '@nestjs/common';
import {MongooseModule} from '@nestjs/mongoose';
import {UserService} from './user.service';
import {UserSchema} from "./schemas/user.schema";
import {UserController} from './user.controller';
import {CartService} from "../cart/cart.service";
import {CartModule} from "../cart/cart.module";

@Module({
    imports: [
        MongooseModule.forFeature([{name: 'User', schema: UserSchema}]),
        CartModule,
    ],
    providers: [
        UserService,
        CartService
    ],
    controllers: [UserController],
    exports: [
        MongooseModule.forFeature([{name: 'User', schema: UserSchema}]),
        UserService,
    ],
})
export class UserModule {
}
