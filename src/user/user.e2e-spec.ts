import {INestApplication} from "@nestjs/common";
import {UserController} from "./user.controller";
import {UserService} from "./user.service";
import {Test} from "@nestjs/testing";
import {MongooseModule} from "@nestjs/mongoose";
import {UserSchema} from "./schemas/user.schema";
import * as request from "supertest";
import assert = require("assert");
import {CartModule} from "../cart/cart.module";
import {CartService} from "../cart/cart.service";
import {UserModule} from "./user.module";

describe('User', () => {
    let app: INestApplication;
    let controller: UserController;
    let service: UserService;

    let toDeleteUserId = undefined;

    beforeAll(async () => {
        const moduleRef = await Test.createTestingModule({
            imports: [
                MongooseModule.forRoot(process.env.MONGO_URL, {
                    useFindAndModify: false,
                }),
                UserModule,
                CartModule,
            ],
            controllers: [UserController],
            providers: [UserService, CartService],
        })
            .compile();

        app = moduleRef.createNestApplication();
        controller = app.get<UserController>(UserController);
        service = app.get<UserService>(UserService);
        await app.init();
    });

    it('/GET user', async () => {
        const expectedResult = await service.findAll();
        return request(app.getHttpServer())
            .get('/user')
            .expect(200)
            .expect(JSON.stringify(expectedResult));
    })

    it('/POST user', async () => {
        return request(app.getHttpServer())
            .post('/user')
            .send({
                lastname: "a",
                firstname: "a",
                password: "a",
                city: "a",
                address: "a",
                phone: "a",
                mail: "fakeemail@fake.com",
                status: "a",
            })
            .expect(201)
            .then(async response => {
                assert(await service.find(response.body._id) !== null);
                toDeleteUserId = response.body._id;
            });
    });

    it('/PUT user', async () => {
        const shops = await service.findAll();
        return request(app.getHttpServer())
            .put('/user/' + toDeleteUserId)
            .send({
                lastname: "b",
                firstname: "b",
                password: "b",
                city: "b",
                address: "b",
                phone: "b",
                mail: "fakeemail@fake.fr",
                status: "b",
            })
            .expect(200)
            .then(async response => {
                const user = await service.find(response.body._id);
                assert(user.lastname === 'b');
                assert(user.firstname === 'b');
                assert(user.password === 'b');
                assert(user.city === 'b');
                assert(user.address === 'b');
                assert(user.phone === 'b');
                assert(user.mail === "fakeemail@fake.fr");
                assert(user.status === 'b');
            })
    });

    it('/DELETE user', async () => {
        return request(app.getHttpServer())
            .delete('/user/' + toDeleteUserId)
            .expect(200)
            .then(async response => {
                const user = await service.find(toDeleteUserId);
                assert(user === null);
                toDeleteUserId = undefined;
            })
    })

    afterAll(async () => {
        if (toDeleteUserId) {
            await service.delete(toDeleteUserId);
        }
        await app.close();
    });
})
