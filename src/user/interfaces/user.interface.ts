import {Document} from 'mongoose';

export interface User extends Document {
    lastname: string;
    firstname: string;
    password: string;
    city: string;
    address: string;
    phone: string;
    mail: string;
    status: string;
    readonly __v: number;
    readonly _id: string;
}
