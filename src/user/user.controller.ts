import {Body, Controller, Delete, Get, NotFoundException, Param, Post, Put, Res} from '@nestjs/common';
import {UserService} from "./user.service";
import {CreateUserDto} from "./dto/create-user.dto";
import {ApiQuery, ApiTags} from "@nestjs/swagger";
import {CartService} from "../cart/cart.service";

@ApiTags('User')
@Controller('user')
export class UserController {
    constructor(
        private userService: UserService,
        private cartService: CartService,
    ) {
    }

    @Get()
    async getAllUser(@Res() res) {
        const users = await this.userService.findAll();
        return res.json(users);
    }

    @Post()
    async createUser(@Res() res, @Body() createUserDto: CreateUserDto) {
        const user = await this.userService.create(createUserDto);
        return res.json(user);
    }

    @Get(':id')
    async getUser(@Param('id') id: string, @Res() res) {
        const user = await this.userService.find(id);
        if (user === null) {
            throw new NotFoundException('User: \'' + id + '\' doesn\'t exist.');
        }
        return res.json(user);
    }

    @ApiQuery({name: 'populate', type: 'boolean', required: false})
    @Get(':id/validatedCarts')
    async getUserValidatedCarts(@Param('id') id: string, @Res() res) {
        const carts = await this.cartService.findValidatedByUser(id);
        return res.json(carts);
    }

    @Delete(':id')
    async deleteUser(@Param('id') id: string, @Res() res) {
        const deletedUser = await this.userService.delete(id);
        if (deletedUser.deletedCount === 1) {
            return res.json({
                message: 'User have been deleted',
            });
        } else {
            throw new NotFoundException('User: \'' + id + '\' doesn\'t exist.');
        }
    }

    @Put(':id')
    async updateUser(@Param('id') id: string, @Body() createUserDto: CreateUserDto, @Res() res) {
        const updatedUser = await this.userService.update(id, createUserDto);
        if (updatedUser === null) {
            throw new NotFoundException('User: \'' + id + '\' doesn\'t exist.');
        } else {
            return res.json(updatedUser);
        }
    }
}
