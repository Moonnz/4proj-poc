module.exports = {
  apps : [{
    name: '4proj-poc',
    script: './main.js',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '200M',
    env: {
      NODE_ENV: 'production',
      MONGO_URL: 'mongodb+srv://4proj:viHzcp7jTTooNYiF@4dvop-odrem.gcp.mongodb.net/jest?retryWrites=true&w=majority&connectTimeoutMS=1000',
      VERSION: '1.1.0',
      BUILD_DATE: '17:00 10/06/2020'
    },
  }],
};
